<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2023-10-01 21:12:49 by adalbertofontoura-->
<display version="2.0.0">
  <name>PV_VALVE_BlockIcon_Vertical_Left_Compact</name>
  <width>153</width>
  <height>182</height>
  <background_color>
    <color red="220" green="225" blue="221" alpha="0">
    </color>
  </background_color>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle</name>
    <x>66</x>
    <y>36</y>
    <width>20</width>
    <height>30</height>
    <line_width>0</line_width>
    <line_color>
      <color name="Group Background" red="200" green="205" blue="201">
      </color>
    </line_color>
    <background_color>
      <color name="Group Background" red="200" green="205" blue="201">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon</name>
    <text>E</text>
    <x>61</x>
    <y>29</y>
    <width>30</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="29.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value></value>
        </exp>
        <exp bool_exp="pv1 == true">
          <value>M</value>
        </exp>
        <exp bool_exp="pv2 == true">
          <value>F</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Auto</pv_name>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Manual</pv_name>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Forced</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_TitleLBL</name>
    <text>${WIDDev}-${WIDIndex}</text>
    <x>7</x>
    <width>143</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <tooltip>Device name</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_Interlock</name>
    <symbols>
      <symbol>../../../symbols/Interlock/interlock.svg</symbol>
    </symbols>
    <x>107</x>
    <y>36</y>
    <width>30</width>
    <height>30</height>
    <disconnect_overlay_color>
      <color red="149" green="110" blue="221" alpha="0">
      </color>
    </disconnect_overlay_color>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:GroupInterlock</pv_name>
      </rule>
    </rules>
    <tooltip>Interlock event occured!</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <fallback_symbol>../../../symbols/Interlock/Interlock-Invalid.svg</fallback_symbol>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon</name>
    <symbols>
      <symbol>../../../symbols/Error/Error.svg</symbol>
    </symbols>
    <x>16</x>
    <y>36</y>
    <width>30</width>
    <height>30</height>
    <disconnect_overlay_color>
      <color red="149" green="110" blue="221" alpha="0">
      </color>
    </disconnect_overlay_color>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:GroupAlarm</pv_name>
      </rule>
    </rules>
    <tooltip>Alarm event occured!</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <fallback_symbol>../../../symbols/Error/Error-Invalid.svg</fallback_symbol>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>ManualOpen</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:Cmd_ManuOpen</pv_name>
    <text>Open</text>
    <x>12</x>
    <y>147</y>
    <width>50</width>
    <height>25</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <rules>
      <rule name="Visibility Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:EnableBlkOpen</pv_name>
      </rule>
    </rules>
    <tooltip>Manual Open</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>ManualClose</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:Cmd_ManuClose</pv_name>
    <text>Close</text>
    <x>92</x>
    <y>147</y>
    <width>50</width>
    <height>25</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <rules>
      <rule name="Visibility Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:EnableBlkClose</pv_name>
      </rule>
    </rules>
    <tooltip>Manual Close</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_CenterIconSolenoid_1</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:SolenoidColor</pv_name>
    <symbols>
      <symbol>../../../symbols/3-Way-Pneumatic-Valve_v01/3-Way-Pneumatic-Valve-Solenoid_Open.svg</symbol>
      <symbol>../../../symbols/2-Way-Pneumatic-Valve_v01/2-Way-Pneumatic-Valve-Solenoid_Close.svg</symbol>
      <symbol>../../../symbols/2-Way-Pneumatic-Valve_v01/2-Way-Pneumatic-Valve-Solenoid_Not-Controlled.svg</symbol>
      <symbol>../../../symbols/2-Way-Pneumatic-Valve_v01/2-Way-Pneumatic-Valve-Solenoid_MAJOR-ERROR.svg</symbol>
      <symbol>../../../symbols/3-Way-Pneumatic-Valve_v01/3-Way-Pneumatic-Valve-Solenoid_OK.svg</symbol>
      <symbol>../../../symbols/2-Way-Pneumatic-Valve_v01/2-Way-Pneumatic-Valve-Solenoid_BLUE.svg</symbol>
      <symbol>../../../symbols/2-Way-Pneumatic-Valve_v01/2-Way-Pneumatic-Valve-Solenoid_MASKED.svg</symbol>
      <symbol>../../../symbols/2-Way-Pneumatic-Valve_v01/2-Way-Pneumatic-Valve-Solenoid_OFF.svg</symbol>
      <symbol>../../../symbols/2-Way-Pneumatic-Valve_v01/2-Way-Pneumatic-Valve-Solenoid_OK.svg</symbol>
    </symbols>
    <x>28</x>
    <y>74</y>
    <width>79</width>
    <height>66</height>
    <rotation>270.0</rotation>
    <disconnect_overlay_color>
      <color red="149" green="110" blue="221" alpha="0">
      </color>
    </disconnect_overlay_color>
    <actions execute_as_one="true">
    </actions>
    <tooltip>Open faceplate
$(pv_name)
$(pv_value)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <fallback_symbol>../../../symbols/2-Way-Pneumatic-Valve_v01/2-Way-Pneumatic-Valve-Solenoid_INVALID.svg</fallback_symbol>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_CenterIcon_1</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:ValveColor</pv_name>
    <symbols>
      <symbol>../../../symbols/2-Way-Pneumatic-Valve_v01/2-Way-Pneumatic-Valve_Open.svg</symbol>
      <symbol>../../../symbols/2-Way-Pneumatic-Valve_v01/2-Way-Pneumatic-Valve_MINOR-ERROR.svg</symbol>
      <symbol>../../../symbols/2-Way-Pneumatic-Valve_v01/2-Way-Pneumatic-Valve_Ok.svg</symbol>
    </symbols>
    <x>30</x>
    <y>74</y>
    <width>66</width>
    <height>66</height>
    <rotation>270.0</rotation>
    <disconnect_overlay_color>
      <color red="149" green="110" blue="221" alpha="0">
      </color>
    </disconnect_overlay_color>
    <actions execute_as_one="true">
    </actions>
    <tooltip>Open faceplate
$(pv_name)
$(pv_value)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <fallback_symbol>../../../symbols/2-Way-Pneumatic-Valve_v01/2-Way-Pneumatic-Valve_INVALID.svg</fallback_symbol>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>WID_OpenFaceplate</name>
    <actions>
      <action type="open_display">
        <file>../faceplates/PV_VALVE_Faceplate.bob</file>
        <macros>
          <Dev>${WIDDev}</Dev>
          <Dis>${WIDDis}</Dis>
          <Index>${WIDIndex}</Index>
          <SecSub>${WIDSecSub}</SecSub>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <width>153</width>
    <height>147</height>
    <transparent>true</transparent>
    <tooltip>Open faceplate</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
</display>
